module.exports = {
  env: {
    "es6": true,
    "node": true,
    "mocha": true
  },
  rules: {
    "linebreak-style": [
      "error",
      "unix"
    ],
    "object-curly-spacing": [
      "warn",
      "always"
    ],
    "eol-last": [
      "warn",
      "always"
    ],
    "complexity": [
      "warn",
      10
    ],
    "comma-dangle": [
      "warn",
      "always-multiline"
    ],
    "no-plusplus": "warn",
    "no-restricted-globals": "warn",
    "no-restricted-syntax": "warn",
    "class-methods-use-this": "warn",
    "prefer-const": "warn",
    "no-await-in-loop": "warn",
    "guard-for-in": "warn",
    "no-shadow": "warn",
    "no-param-reassign": "warn",
    "no-else-return": "warn",
    "no-prototype-builtins": "warn",
    "max-len": [
      "warn",
      {
        "code": 120,
        "tabWidth": 2,
        "ignoreComments": false,
        "ignoreUrls": true,
        "ignoreStrings": true,
        "ignoreTemplateLiterals": true,
        "ignoreRegExpLiterals": true
      }
    ],
    "eqeqeq": "warn",
    "no-multiple-empty-lines": [
      "warn",
      {
        "max": 2,
        "maxEOF": 1,
        "maxBOF": 0
      }
    ],
    "require-atomic-updates": "warn",
    "no-console": "warn",
    "arrow-parens": [
      "warn",
      "as-needed"
    ],
    "padding-line-between-statements": [
      "warn",
      {
        "blankLine": "always",
        "prev": "*",
        "next": "return"
      }
    ],
    "space-in-parens": [
      "warn",
      "never"
    ],
    "keyword-spacing": [
      "warn",
      {
        "before": true,
        "after": true
      }
    ],
    "array-bracket-spacing": [
      "warn",
      "never"
    ],
    "arrow-spacing": [
      "warn",
      {
        "before": true,
        "after": true
      }
    ],
    "block-spacing": [
      "warn",
      "always"
    ],
    "computed-property-spacing": [
      "warn",
      "never"
    ],
    "template-curly-spacing": [
      "warn",
      "always"
    ],
    "no-trailing-spaces": "warn",
    "radix": [
      "warn",
      "as-needed"
    ],
    "space-before-function-paren": ["warn", "never"],
    "no-dupe-class-members": "warn",
    "func-call-spacing": ["warn", "never"],
    "no-extra-parens": "warn",
    "no-unused-vars": "warn",
    "global-require": "warn",
    "no-extra-semi": "warn",
    "comma-spacing": ["warn", { "before": false, "after": true }],
    "require-await": "warn",
    "brace-style": ["warn", "1tbs"],
    "camelcase": ["warn", { "properties": "never", "ignoreDestructuring": true }],
    "indent": ["warn", 2, { "SwitchCase": 1 }],
    "quotes": ["warn", "single"],
    "semi": ["warn", "always"],
  },
  overrides: [{
    files: [
      "./**/*.ts"
    ],
    parser: "@typescript-eslint/parser",
    parserOptions: {
      tsconfigRootDir: __dirname,
      project: [
        "./tsconfig.json",
        "./tsconfig.test.json"
      ]
    },
    plugins: [
      "@typescript-eslint"
    ],
    extends: [
      "eslint:recommended",
      "plugin:@typescript-eslint/eslint-recommended",
      "plugin:@typescript-eslint/recommended",
      "plugin:@typescript-eslint/recommended-requiring-type-checking"
    ],
    rules: {
      "space-before-function-paren": "off",
      "no-dupe-class-members": "off",
      "func-call-spacing": "off",
      "no-extra-parens": "off",
      "no-unused-vars": "off",
      "global-require": "off",
      "no-extra-semi": "off",
      "comma-spacing": "off",
      "require-await": "off",
      "brace-style": "off",
      "camelcase": "off",
      "indent": "off",
      "quotes": "off",
      "semi": "off",
      "@typescript-eslint/space-before-function-paren": ["warn", "never"],
      "@typescript-eslint/no-dupe-class-members": "warn",
      "@typescript-eslint/func-call-spacing": ["warn", "never"],
      "@typescript-eslint/no-extra-parens": "warn",
      "@typescript-eslint/no-unused-vars": "warn",
      "@typescript-eslint/no-extra-semi": "warn",
      "@typescript-eslint/comma-spacing": ["warn", { "before": false, "after": true }],
      "@typescript-eslint/require-await": "warn",
      "@typescript-eslint/brace-style": ["warn", "1tbs"],
      "@typescript-eslint/camelcase": ["warn", { "properties": "never", "ignoreDestructuring": true }],
      "@typescript-eslint/indent": ["warn", 2, { "SwitchCase": 1 }],
      "@typescript-eslint/quotes": ["warn", "single"],
      "@typescript-eslint/semi": ["warn", "always"],
      "@typescript-eslint/explicit-function-return-type": "warn",
      "@typescript-eslint/no-unnecessary-condition": "warn",
      "@typescript-eslint/no-use-before-define": "off",
      "@typescript-eslint/no-misused-promises": "warn",
      "@typescript-eslint/no-empty-function": "warn",
      "@typescript-eslint/no-explicit-any": "off",
      "@typescript-eslint/await-thenable": "warn",
      "@typescript-eslint/unbound-method": "warn",
      "@typescript-eslint/ban-ts-ignore": "warn",
      "@typescript-eslint/no-this-alias": "warn",
      "@typescript-eslint/prefer-for-of": "warn",
      "@typescript-eslint/unified-signatures": "warn",
    }
  }]
}
