export interface User {
  id: string;
  name: string;
  balance: number;
}

export enum ResponseStatus {
  SUCCESS = 'success',
  ERROR = 'error'
}

export enum UserPrediction {
  HIGH = 'high',
  LOW = 'low'
}
