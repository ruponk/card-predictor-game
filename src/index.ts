import express from 'express';
import bodyParser from 'body-parser';
import bearerToken from 'express-bearer-token';
import config from 'config';

import { init as initSequelize } from './sequelize';
import { setup as setupRouters } from './routers';
import { setup as setupMicroserviceLinks } from './microservice-links';

import { errorHandler } from './middleware';

function setup(): void {
  const port = config.get('port');

  const sequelize = initSequelize();

  setupMicroserviceLinks();

  const app = express();

  app.use(bearerToken());
  app.use(bodyParser.json());

  setupRouters(app, sequelize);

  app.use(errorHandler);

  app.listen(port, function() {
    console.log(`app listening on port ${ port }`);
  });
}


process.on('uncaughtException', (error: Error) => {
  console.error(error);
});
process.on('unhandledRejection', (reason, promise) => {
  console.error({ reason, promise });
});

try {
  setup();
} catch (error) {
  console.error(error);
}

