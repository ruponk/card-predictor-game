import { Wallet } from './wallet';

export {
  Wallet,
};

export function setup(): void {
  Wallet.setup();
}
