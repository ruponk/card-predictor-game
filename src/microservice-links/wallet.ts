import config from 'config';
import request from 'request-promise-native';

import { User, ResponseStatus } from '../types';
import { computeSignature } from '../utils';

export class Wallet {
  private static host: string;
  private static port: string;
  private static secret: string;

  public static setup(): void {
    const { host, port, secret } = config.get('wallet');

    this.host = host;
    this.port = port;
    this.secret = secret;
  }

  public static async verifyToken(token: any): Promise<User | null> {
    const uri = `http://${ this.host }:${ this.port }/api/v1/verify-token`;
    const body = { token };

    try {
      return await request.post(uri, {
        auth: { bearer: computeSignature(body, this.secret) },
        json: true,
        body,
      });
    } catch (error) {
      console.error(error);

      return null;
    }
  }

  public static async withdraw(
    amount: number,
    userId: string
  ): Promise<{ status: ResponseStatus }> {
    const uri = `http://${ this.host }:${ this.port }/api/v1/withdraw`;
    const body = { amount, userId };

    try {
      return await request.post(uri, {
        auth: { bearer: computeSignature(body, this.secret) },
        json: true,
        body,
      });
    } catch (error) {
      console.error(error);

      return { status: ResponseStatus.ERROR };
    }
  }

  public static async deposit(
    amount: number,
    userId: string
  ): Promise<{ status: ResponseStatus }> {
    const uri = `http://${ this.host }:${ this.port }/api/v1/deposit`;
    const body = { amount, userId };

    try {
      return await request.post(uri, {
        auth: { bearer: computeSignature(body, this.secret) },
        json: true,
        body,
      });
    } catch (error) {
      console.error(error);

      return { status: ResponseStatus.ERROR };
    }
  }
}
