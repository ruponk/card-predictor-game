import { Sequelize } from 'sequelize';

import { Round, setup as setupRoundModel } from './round';

export {
  Round,
};

export function setup(sequelize: Sequelize): void {
  setupRoundModel(sequelize);

  Object.keys(sequelize.models).forEach(modelName => {
    const model: any = sequelize.models[modelName];

    if (typeof model.associate === 'function') {
      model.associate(sequelize.models);
    }
  });
}
