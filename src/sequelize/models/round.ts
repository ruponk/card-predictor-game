import { Sequelize, Model, FindOptions, Transaction, InstanceUpdateOptions } from 'sequelize';

import { roundDefinition } from './definitions';
import { RoundStatus } from './types';

export class Round extends Model {
  public id!: string;
  public userId!: string;
  public deckSeed!: string;
  public betAmount!: number;
  public status!: RoundStatus;

  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;

  public isActive(): boolean {
    return this.status === RoundStatus.ACTIVE;
  }

  public async close(options: InstanceUpdateOptions = {}): Promise<void> {
    await this.update({ status: RoundStatus.CLOSED }, options);
  }

  public async void(options: InstanceUpdateOptions = {}): Promise<void> {
    await this.update({ status: RoundStatus.VOIDED }, options);
  }

  public static async findActiveByUserId(
    userId: string,
    options: FindOptions = {}
  ): Promise<Round | null> {
    return this.findOne({
      ...options,
      where: { userId, status: RoundStatus.ACTIVE },
    });
  }

  public static async findAndLockById(
    id: string,
    options: FindOptions & { transaction: Transaction }
  ): Promise<Round | null> {
    return this.findOne({
      ...options,
      where: { id },
      lock: Transaction.LOCK.UPDATE,
    });
  }
}

export function setup(sequelize: Sequelize): void {
  const { attributes, options } = roundDefinition;
  Round.init(attributes, { ...options, sequelize });
}
