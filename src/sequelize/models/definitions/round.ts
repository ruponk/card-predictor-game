import { DataTypes } from 'sequelize';

import { ModelDefinition, RoundStatus } from '../types';

export const roundDefinition: ModelDefinition = {
  attributes: {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV1,
    },
    userId: {
      type: DataTypes.STRING,
      field: 'user_id',
      allowNull: false,
    },
    deckSeed: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    betAmount: {
      type: DataTypes.INTEGER,
      field: 'bet_amount',
      allowNull: false,
      defaultValue: 0,
    },
    status: {
      type: DataTypes.ENUM(...Object.values(RoundStatus)),
      allowNull: false,
      defaultValue: RoundStatus.ACTIVE,
    },
    createdAt: {
      type: DataTypes.DATE,
      field: 'created_at',
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
    updatedAt: {
      type: DataTypes.DATE,
      field: 'updated_at',
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
  },
  options: {
    tableName: 'rounds',
    modelName: 'Round',
    freezeTableName: true,
    timestamps: false,
  },
};
