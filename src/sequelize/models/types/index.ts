import { ModelAttributes, ModelOptions } from 'sequelize';

export interface ModelDefinition {
  attributes: ModelAttributes;
  options: ModelOptions;
}

export enum RoundStatus {
  ACTIVE = 'active',
  CLOSED = 'closed',
  VOIDED = 'voided'
}
