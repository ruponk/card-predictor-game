import config from 'config';

// NOTE(roman): this is separated so it can be used by the sequelize-cli
const sequelizeConfig: any = config.get('sequelize');
const {
  username,
  password,
  database,
  host,
  port,
  dialect,
  logging,
} = sequelizeConfig;

// NOTE(roman): the sequelize-cli does not like the default exports from ts
export {
  username,
  password,
  database,
  host,
  port,
  dialect,
  logging,
};
