import { Sequelize } from 'sequelize';

import * as config from './config';
import { setup as setupModels } from './models';

export function init(): Sequelize {
  const { database, username, password } = config;
  const sequelize = new Sequelize(database, username, password, config);

  setupModels(sequelize);

  return sequelize;
}
