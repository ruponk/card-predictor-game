import { QueryInterface, DataTypes } from 'sequelize';

const tableName = 'rounds';

export async function up(queryInterface: QueryInterface): Promise<void> {
  await queryInterface.createTable(tableName, {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV1,
    },
    userId: {
      type: DataTypes.STRING,
      field: 'user_id',
      allowNull: false,
    },
    deckSeed: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    betAmount: {
      type: DataTypes.INTEGER,
      field: 'bet_amount',
      allowNull: false,
      defaultValue: 0,
    },
    status: {
      type: DataTypes.ENUM('active', 'closed', 'voided'),
      allowNull: false,
      defaultValue: 'active',
    },
    createdAt: {
      type: DataTypes.DATE,
      field: 'created_at',
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
    updatedAt: {
      type: DataTypes.DATE,
      field: 'updated_at',
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
  });

}

export async function down(queryInterface: QueryInterface): Promise<void> {
  await queryInterface.dropTable(tableName);
}
