import { FeathersError } from '@feathersjs/errors';
import { Request, Response, NextFunction } from 'express';
import { ValidationError } from 'sequelize';

import { ResponseStatus } from '../types';


export function errorHandler<T extends Error>(error: T, req: Request, res: Response, next: NextFunction): Response {
  const statusCode = error instanceof ValidationError
    ? 400
    : error instanceof FeathersError ? error.code : 500;

  console.error('error')

  return res.status(statusCode).json({
    status: ResponseStatus.ERROR,
    error,
  });
}

