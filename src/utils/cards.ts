import { create } from 'random-seed';

export enum CardSuit {
  SPADES = '♤',
  HEARTS = '♡',
  CLUBS = '♧',
  DIAMONDS = '♢'
}

export enum CardValue {
  ONE = '1',
  TWO = '2',
  THREE = '3',
  FOUR = '4',
  FIVE = '5',
  SIX = '6',
  SEVEN = '7',
  EIGHT = '8',
  NINE = '9',
  TEN = '10',
  JACK = 'J',
  QUEEN = 'Q',
  KING = 'K',
  ACE = 'A'
}
export class Card {
  constructor(
    public value: CardValue,
    public suit: CardSuit,
  ) { }

  // eslint-disable-next-line complexity
  public valueOf(): number {
    switch (this.value) {
      case CardValue.ONE: return 1;
      case CardValue.TWO: return 2;
      case CardValue.THREE: return 3;
      case CardValue.FOUR: return 4;
      case CardValue.FIVE: return 5;
      case CardValue.SIX: return 6;
      case CardValue.SEVEN: return 7;
      case CardValue.EIGHT: return 8;
      case CardValue.NINE: return 9;
      case CardValue.TEN: return 10;
      case CardValue.JACK: return 11;
      case CardValue.QUEEN: return 12;
      case CardValue.KING: return 13;
      case CardValue.ACE: return 14;
      default: return 0;
    }
  }

  public toString(): string {
    return `${ this.suit }${ this.value }`;
  }
}

export class Deck {
  private static cardGroups = Object.values(CardValue).reduce(
    (cardGroups: Array<Array<Card>>, value: CardValue) => [
      ...cardGroups,
      Object.values(CardSuit).reduce(
        (cards: Card[], suit: CardSuit) => [...cards, new Card(value, suit)],
        []
      ),
    ],
    []
  );

  public static draw(count: number, seed: string): Array<Card> {
    const cardGroups = [...this.cardGroups];

    const baseRNG = create(seed);
    const cardGroupRNG = create(baseRNG.random().toString(2));
    const cardRNG = create(baseRNG.random().toString(2));

    const clampedCount = count  > cardGroups.length ? cardGroups.length : count;

    const randomCards = new Array<Card>();
    for (let i = 0; i < clampedCount; i += 1) {
      const randomCardGroupIndex = cardGroupRNG.range(cardGroups.length);
      const randomCardGroup = cardGroups[randomCardGroupIndex];
      const randomCardIndex = cardRNG.range(randomCardGroup.length);

      randomCards.push(randomCardGroup[randomCardIndex]);

      cardGroups.splice(randomCardGroupIndex, 1);
    }

    return randomCards;
  }
}
