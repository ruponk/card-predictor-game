import { createHash } from 'crypto';

export * from './cards';

export function computeSignature(data: any, secret: string): string {
  const dataKeys = Object.keys(data).sort();

  const orderedData = dataKeys.reduce((acc: any, key: string) => ({ ...acc, [key]: data[key] }), {});
  const stringifiedData = JSON.stringify(orderedData);

  const dataHash = createHash('sha1').update(stringifiedData).digest('hex');
  const dataSignature = createHash('sha1').update(dataHash + secret).digest('hex');

  return dataSignature;
}
