import { Express } from 'express';
import { Sequelize } from 'sequelize';

import { setup as setupAuthenticateRouter } from './authenticate';
import { setup as setupStartGameRouter } from './start-game';
import { setup as setupEndGameRouter } from './end-game';

export function setup(app: Express, sequelize: Sequelize): void {
  setupAuthenticateRouter(app);
  setupStartGameRouter(app, sequelize);
  setupEndGameRouter(app, sequelize);
}

