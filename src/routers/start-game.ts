import { Express, Router, Request, Response, NextFunction } from 'express';
import { BadRequest, NotFound } from '@feathersjs/errors';
import { Sequelize } from 'sequelize';

import { Round } from '../sequelize/models';
import { ResponseStatus } from '../types';
import { Deck, Card } from '../utils';

let _sequelize: Sequelize;

async function _startGame(
  roundId: string,
  betAmount: number
): Promise<Card> {
  const transaction = await _sequelize.transaction();

  try {
    const round = await Round.findAndLockById(roundId, { transaction });
    if (!round) {
      throw new NotFound('Round not found.');
    }

    if (!round.isActive()) {
      throw new BadRequest('Round is not active.');
    }

    await round.update({ betAmount }, { transaction });

    const [card] = Deck.draw(1, round.deckSeed);

    await transaction.commit();

    return card;
  } catch (error) {
    await transaction.rollback();

    throw error;
  }
}

async function startGame(
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> {
  const { body: { roundId, betAmount } } = req;

  try {
    if (!roundId) {
      throw new BadRequest('Missing required round id.');
    }

    if (!betAmount) {
      throw new BadRequest('Missing required bet amount.');
    }

    const parsedBetAmount = parseInt((betAmount * 100).toFixed(0));
    if (!parsedBetAmount || parsedBetAmount <= 0) {
      throw new BadRequest('Invalid bet amount.');
    }

    const card = await _startGame(roundId, parsedBetAmount);

    res.json({
      status: ResponseStatus.SUCCESS,
      card: card.toString(),
    });

    return next();
  } catch (error) {
    return next(error);
  }
}

export function setup(app: Express, sequelize: Sequelize): void {
  _sequelize = sequelize;

  const router = Router();

  router.route('/').post(startGame);

  app.use('/api/v1/start-game', router);
}
