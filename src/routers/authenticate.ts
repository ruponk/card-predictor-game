import { Express, Router, Request, Response, NextFunction } from 'express';
import { NotAuthenticated } from '@feathersjs/errors';
import { v4 } from 'uuid';

import { Round } from '../sequelize/models';
import { Wallet } from '../microservice-links';
import { ResponseStatus } from '../types';

async function authenticate(
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> {
  const { token } = req;
  try {
    if (!token) {
      throw new NotAuthenticated('Missing required token');
    }

    const user = await Wallet.verifyToken(token);
    if (!user) {
      throw new NotAuthenticated('Provided token is invalid');
    }

    const unfinishedRound = await Round.findActiveByUserId(user.id);
    const round = unfinishedRound || await Round.create({ userId: user.id, deckSeed: v4() });

    res.json({
      status: ResponseStatus.SUCCESS,
      user,
      round: {
        id: round.id,
      },
    });

    return next();
  } catch (error) {
    return next(error);
  }
}

export function setup(app: Express): void {
  const router = Router();

  router.route('/').post(authenticate);

  app.use('/api/v1/authenticate', router);
}
