import { Express, Router, Request, Response, NextFunction } from 'express';
import { BadRequest, NotFound, GeneralError, convert } from '@feathersjs/errors';
import { Sequelize } from 'sequelize';

import { Wallet } from '../microservice-links';
import { Round } from '../sequelize/models';
import { ResponseStatus, UserPrediction } from '../types';
import { Deck, Card } from '../utils';

function isCorrectPrediction(
  prediction: UserPrediction,
  firstCard: Card,
  secondCard: Card
): boolean {
  return firstCard < secondCard && prediction === UserPrediction.HIGH
    || firstCard > secondCard && prediction === UserPrediction.LOW;
}

let _sequelize: Sequelize;

async function _endGame(
  roundId: string,
  prediction: UserPrediction,
): Promise<{ code: number; message: string }> {
  const transaction = await _sequelize.transaction();
  try {
    const round = await Round.findAndLockById(roundId, { transaction });
    if (!round) {
      throw new NotFound('Round not found.');
    }
    if (!round.isActive()) {
      throw new BadRequest('Round is not active.');
    }

    const [firstCard, secondCard] = Deck.draw(2, round.deckSeed);

    const response = { code: 0, message: 'stub' };

    const withdrawResponse = await Wallet.withdraw(round.betAmount, round.userId);
    if (withdrawResponse.status === ResponseStatus.ERROR) {
      await round.void({ transaction });

      response.code = 400;
      response.message = 'Not enough balance.';
    } else if (!isCorrectPrediction(prediction, firstCard, secondCard)) {
      await round.close({ transaction });

      response.code = 200;
      response.message = `Wrong, it was ${ secondCard.toString() }.`;
    } else {
      const depositResponse = await Wallet.deposit(round.betAmount * 2, round.userId);
      if (depositResponse.status === ResponseStatus.ERROR) {
        throw new GeneralError('There was a problem with depositing the win. Try again.');
      }

      await round.close({ transaction });

      response.code = 200;
      response.message = `Correct, it was ${ secondCard.toString() }.`;
    }

    await transaction.commit();

    return response;
  } catch (error) {

    await transaction.rollback();

    throw error;
  }
}

async function endGame(
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> {
  const { body } = req;
  const { roundId, prediction } = body;

  try {
    if (!roundId) {
      throw new BadRequest('Missing required round id.');
    }
    if (!prediction) {
      throw new BadRequest('Missing required prediction.');
    }
    if (!Object.values(UserPrediction).includes(prediction)) {
      throw new BadRequest('Invalid prediction.');
    }

    const { code, message } = await _endGame(roundId, prediction);

    const isError = code >= 400;

    if (!isError) {
      res.status(code).json({ status: ResponseStatus.SUCCESS, message });
    }

    return isError ? next(convert({ message, name: String(code) })) : next();
  } catch (error) {

    return next(error);
  }
}

export function setup(app: Express, sequelize: Sequelize): void {
  _sequelize = sequelize;

  const router = Router();

  router.route('/').post(endGame);

  app.use('/api/v1/end-game', router);
}
