module.exports = {
  port: parseInt(process.env.GAME_PORT) || 8081,
  sequelize: {
    username: process.env.GAME_MYSQL_USER || 'root',
    password: process.env.GAME_MYSQL_PASSWORD || 'secretpassword',
    database: process.env.GAME_MYSQL_DATABASE || 'game',
    host: process.env.GAME_MYSQL_HOST || '127.0.0.1',
    port: process.env.GAME_MYSQL_PORT || '3306',
    dialect: 'mysql',
    logging: false,
  },
  wallet: {
    host: process.env.GAME_WALLET_HOST || 'localhost',
    port: process.env.GAME_WALLET_PORT || '8081',
    secret: process.env.GAME_WALLET_SECRET || 'verysecretsecret'
  }
};
