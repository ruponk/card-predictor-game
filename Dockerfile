FROM node:erbium as build

WORKDIR /usr/app

COPY src ./src
COPY test ./test
COPY config ./config
COPY package.json ./package.json
COPY package-lock.json ./package-lock.json
COPY tsconfig.json ./tsconfig.json
COPY tsconfig.test.json ./tsconfig.test.json
COPY .eslintrc.js ./.eslintrc.js
COPY .eslintignore ./.eslintignore

RUN npm ci
RUN npm t
RUN npm run build
RUN npm prune "--production"

FROM node:erbium-alpine

COPY --from=build /usr/app /usr/app

WORKDIR /usr/app

ENV PORT 8080
EXPOSE 8080

CMD [ "npm", "start" ]
