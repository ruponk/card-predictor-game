import assert from 'assert';

import { Deck, computeSignature } from '../src/utils';

describe('utils computeSignature', function () {
  it('should compute the signature with a defined secret', function () {
    const signature = computeSignature({ message: 'test' }, 'secret');

    assert.strictEqual(signature, 'dd0a6c4f8bd643c26d9e38ad553d5f958f5e40da');
  })
  it('should compute the signature with an empty secret', function () {
    const signature = computeSignature({ message: 'test' }, '');

    assert.strictEqual(signature, 'd4da3c6ca19790a0f6da27ab0ce235ef12119faf');
  })
  it('should compute the signature for an empty object', function () {
    const signature = computeSignature({}, 'secret');

    assert.strictEqual(signature, 'b5c1ca151eb752c727fc96427ce7f768cfb81413');
  })
})

describe('utils Deck', function () {
  it('should draw same cards for the same seeds', function () {
    const cards1 = Deck.draw(4, 'seed');
    const cards2 = Deck.draw(4, 'seed');

    assert.deepStrictEqual(cards1, cards2);
  })
  it('should draw different cards for different seeds', function () {
    const cards1 = Deck.draw(4, 'seed1');
    const cards2 = Deck.draw(4, 'seed2');

    assert.notDeepStrictEqual(cards1, cards2);
  })
})
